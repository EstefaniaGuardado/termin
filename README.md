# Termin #
Termin helps organise your meetings, improving the hour of meeting when exist guests around the world and it's difficult to set a good hour. Termin provides a proposal of hour to improve this problem, and offers other features for handler the meetings.

Temin is developed in Objective for iOS platform.

For more information about the project take a look in the actual [repository](https://github.com/iTermin/app_iOS) in Github.

## License ##
[Apache Version 2.0](https://github.com/iTermin/app_iOS/blob/github/%231_README-Wiki/License)